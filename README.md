# The Tau Manifesto

This is the LaTeX source of [*The Tau Manifesto*](https://tauday.com/tau-manifesto). Copyright © 2010–2017 by Michael Hartl.

Here are the steps for translating the manifesto into a language other than English:

2. Fork the repository and add me (`mhartl`) as a collaborator.
3. Install the `softcover` Ruby gem and get the document to build locally by following the instructions in [*The Softcover Book*](http://manual.softcover.io/book). (If your system isn't already configured as a Ruby development environment, see [*Learn Enough Dev Environment to Be Dangerous*](https://www.learnenough.com/dev-environment-tutorial).)
4. Edit the file `chapters/tau_manifesto.tex` to make the translation. See especially the section [Foreign language support](http://manual.softcover.io/book/customization#sec-foreign_language) in the Softcover manual.
6. Once the translation is nearing completion, send email to michael@michaelhartl.com for further instructions.
